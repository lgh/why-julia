# Welcome to the Git Repository Julia is the Answer but was is the Question?

This repository will contain the slides and materials for the talk, as well as
instructions for a take-home task that you can use to reinforce your
understanding of the concepts presented.

![JuliaCon Local 2023 Logo](https://juliacon.org/assets/local/eindhoven2023/img/juliacon_local_eindhoven_logo.png)

## Slides
Download the slides here <https://codeberg.org/lgh/why-julia/src/branch/main/julia-is-the-answer.pdf> 

# Take Home Optimization Task for JuMP
## Installation
To get started, please follow these steps:

You can download the latest version of Julia from the official website (<https://julialang.org/>). Follow the installation instructions for your operating system.

## Run Pluto
2. Run the command: 
```bash
julia -p 4 -e "using Pluto; Pluto.run()"
```

Open a terminal or command prompt and navigate to the directory where you cloned this repository. Then, run the command above to start the Pluto environment. This will launch an interactive Jupyter notebook in your web browser.
3. Copy the code from the slides and reproduce the demo

Inside the notebook, you'll find a copy of the code used in the presentation. You can copy and paste it into a new cell and run it to see the demos for yourself. Experiment with different inputs and parameters to see how they affect the results.

# Take-Home Task:

Your task is to implement PlutoUI with sliders for adjusting weights as well as changing the constraints from the slides,
so that every criteria weight. These weights should be slider adjustable. Here are
some specific subtasks to help guide your work:

## Better evaluation of criteria for each language:

Currently, the criteria for evaluating languages are hardcoded in the Pluto codebase. 
You task is to create a Dictionary for where the keys are the name of the criteria and
the values signify how important this criteria is.
The goal is, that different groups can see, if they should choose Julia

### Conduct survey about importance of criteria:
Please create a pull request with your new code and tell us, which criteria is most important for your.


### Host interactive notebook:

Set up an interactive notebook using Pluto or another tool of your choice. 
Share the link to the notebook so others can interact with the Pluto interface and explore the effects of different weight combinations.

# Submitting Your Work:

Once you have completed the tasks, please submit your work by creating a pull request against this repository. Include a brief description of what you did and any issues you encountered. We'll review your submission and provide feedback.

# License:

By submitting your work, you agree to license it under the MIT License. This allows anyone to use, modify, and distribute your contributions freely.

Thank you for participating in this project! We look forward to seeing what you come up with.

# Hints
The first hints and notebooks will be published after JuliaCon Local 2023
